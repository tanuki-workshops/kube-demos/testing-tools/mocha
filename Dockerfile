# mocha helper 2020-09-07 by @k33g - https://gitlab.bots.garden
FROM docker:latest

RUN apk --update add --no-cache bash wget curl tar git jq util-linux nodejs npm
RUN npm install -g mocha
RUN npm install -g mocha-junit-reporter

